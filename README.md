
# Analyze canopy photos for gap fraction

This package reads JPEGs, auto-thresholds them, and returns the 'gap fraction' of the canopy photo.

To use,

```
p <- readCanPhoto("canopyphoto1.jpg")
f <- findThreshold(p)
gapfraction(f)
```

To inspect the thresholding, you can do:
```
plot(f)
```

See `?findThreshold` for details on the automatic thresholding algorithm.

## Installation

To install this package, use the following command. Windows users must have [Rtools](http://cran.r-project.org/bin/windows/Rtools/) installed.

```
library(devtools)
install_bitbucket("remkoduursma/canopyphoto")
```

You also need the `rtiff`, `jpeg` and `reshape` packages:

```
install.packages(c("rtiff","jpeg","reshape"))
```

