#'@export
writeThresh <- function(f, filename){
  
  B <- f$image[,,3]
  B[B < f$thresh] <- 0
  B[B > f$thresh] <- 255
  fim <- imagedata(B, type="grey")
  writeJpeg(filename, fim)
}